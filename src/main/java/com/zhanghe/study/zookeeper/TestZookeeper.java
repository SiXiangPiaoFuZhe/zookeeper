package com.zhanghe.study.zookeeper;

import org.apache.zookeeper.*;
import org.apache.zookeeper.data.Stat;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

/**
 * @author zh
 * @date 2021/4/7 15:46
 */
public class TestZookeeper {

    // 连接地址
    private final String connect = "localhost:2181";

    // 单位是毫秒
    private final int sessionTimeOut = 2000;

    private ZooKeeper zkClient;

    @Before
    public void init() throws IOException {
        // connectString  连接地址，如果是集群的话则使用逗号拼接  如 "127.0.0.1:3000,127.0.0.1:3001,127.0.0.1:3002"

        // sessionTimeout  session过期时间，单位毫秒
        // watcher     监听事件
        zkClient = new ZooKeeper(connect, sessionTimeOut, watchedEvent -> {

        });
    }

    // 创建节点
    @Test
    public void createNode() throws KeeperException, InterruptedException {

        // path  the path for the node  路径
        // data  the initial data for the node  数据
        // acl   the acl for the node  访问权限控制列表
        // createMode specifying whether the node to be created is ephemeral
        //     *                and/or sequential   创建模式，是临时节点/持久化节点 或者是否有序，如果是临时节点，该节点不允许有孩子节点
        String path = zkClient.create("/zk_client_test","第一次创建节点".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
        System.out.println(path);
    }

    // 设置节点数据
    @Test
    public void setNodeData() throws KeeperException, InterruptedException {
        // path  路径
        // data  the data to set  数据
        // version  所期待的版本，相当于乐观锁，如果是-1，则匹配所有版本
        Stat stat = zkClient.setData("/zk_client_test","修改节点数据".getBytes(), 0);
        System.out.println(stat);
    }

    // 获取节点数据
    @Test
    public void getNodeData() throws KeeperException, InterruptedException {

        Stat stat = new Stat();
        //path the given path 路径
        //watch whether need to watch this node 是否监听该节点
        //stat the stat of the node  获取状态信息
        byte[] data = zkClient.getData("/zk_client_test",false,stat);
        System.out.println(new String(data));
        System.out.println(stat);
    }

    // 获取子节点
    @Test
    public void getChildNodes() throws KeeperException, InterruptedException {
        // 第二个参数为是否监听节点变化
        List<String> nodes = zkClient.getChildren("/zookeeper",false);
        for(String node : nodes){
            System.out.println(node);
        }
    }

    // 删除节点
    @Test
    public void deleteNode() throws KeeperException, InterruptedException {
        // path  路径
        // version  所期待的版本，相当于乐观锁，如果是-1，则匹配所有版本
        zkClient.delete("/zk_client_test",-1);
    }

    // 监听机制
    @Test
    public void testWatch(){
        // 实现监听方法
        new Watcher(){

            @Override
            public void process(WatchedEvent event) {
                System.out.println("监听触发"+event);

            }
        };
    }
}
